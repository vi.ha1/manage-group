package database
import (
	"gorm.io/driver/mysql"
	"gorm.io/gorm"
        "fmt"
)

var DB *gorm.DB

func Connect() *gorm.DB{
        connection, err := gorm.Open(mysql.Open("admin:Test@1234@tcp(localhost:3306)/jwt_demo?parseTime=true"), &gorm.Config{})

        if err != nil {
                panic("could not connect to the database")
        }

        DB = connection
        return DB
}

func GetDB() *gorm.DB {
	return DB
}

func CloseDB(){
	sqlDB, err := DB.DB();
	if err != nil {
		fmt.Println(err.Error())
		panic("Can't close to DB!")
	}
	sqlDB.Close()
}