package models

import (
	"github.com/twinj/uuid"
	"manage-group/database"
	"manage-group/auth"
	// "gorm.io/gorm"
)

type Auth struct {
	ID       uint `gorm:"primary_key;auto_increment" json:"id"`
	UserID   uint `gorm:";not null;" json:"user_id"`
	AuthUUID string `gorm:"size:255;not null;" json:"auth_uuid"`
}

func FetchAuth(authD auth.JWTClaim) (*Auth, error) {
	db := database.GetDB()
	au := &Auth{}
	err := db.Debug().Where("user_id = ? AND auth_uuid = ?", authD.UserId, authD.AuthUuid).Take(&au).Error
	if err != nil {
		return nil, err
	}
	return au, nil
}

func FetchAuthExist(userId uint) (*Auth, error) {
	db := database.GetDB()
	au := &Auth{}
	err := db.Debug().Where("user_id = ?", userId).Take(&au).Error
	if err != nil {
		return nil, err
	}
	return au, nil
}

func DeleteAuthExist(userId uint) error {
	db := database.GetDB()
	au := &Auth{}
	err := db.Debug().Where("user_id = ?", userId).Take(&au).Delete(&au)
	if err.Error != nil {
		return db.Error
	}
	return nil
}

//Once the user signup/login, create a row in the auth table, with a new uuid
func CreateAuth(userId uint) (*Auth, error) {
	db := database.GetDB()
	au := &Auth{}
	au.AuthUUID = uuid.NewV4().String() //generate a new UUID each time
	au.UserID = userId
	err := db.Debug().Create(&au).Error
	if err != nil {
		return nil, err
	}
	return au, nil
}

//Once a user row in the auth table
func DeleteAuth(authD auth.JWTClaim) error {
	db := database.GetDB()
	au := &Auth{}
	err := db.Debug().Where("user_id = ? AND auth_uuid = ?", authD.UserId, authD.AuthUuid).Take(&au).Delete(&au)
	if err.Error != nil {
		return db.Error
	}
	return nil
}