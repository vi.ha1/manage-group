package models
import (
	// "jwt-authentication-golang/database"
	"gorm.io/gorm"
	"time"
)

type Group struct {
	gorm.Model
	GroupName string `json:"name" gorm:"unique"`
	OwnerId uint `json:"owner"`
	Members []User `gorm:"many2many:user_groups;"`
}

type UserGroup struct {
	UserId uint `gorm:"primaryKey" column:"user_id" "uniqueIndex:idx_user_group"`
	GroupId uint `gorm:"primaryKey" column:"group_id" "uniqueIndex:idx_user_group"`
	DateJoined time.Time
}
