package models
import (
	"gorm.io/gorm"
	"golang.org/x/crypto/bcrypt"
)
type User struct {
	gorm.Model
	ID uint `gorm:"primary_key"`
	Name     string `json:"name"`
	Username string `json:"username" gorm:"unique"`
	Email    string `json:"email" gorm:"unique"`
	Password string `json:"password"`
	Groups []Group `gorm:"many2many:user_groups"`
}

func (user *User) HashPassword(password string) error {
	bytes, err := bcrypt.GenerateFromPassword([]byte(password), 14)
	if err != nil {
		return err
	}
	user.Password = string(bytes)
	return nil
}

func (user *User) CheckPassword(providePassword string) error {
	err := bcrypt.CompareHashAndPassword([]byte(user.Password), []byte(providePassword))
	if err != nil {
		return err
	}
	return nil
}
