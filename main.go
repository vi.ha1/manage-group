package main
import (
	"manage-group/controllers"
	"manage-group/database"
	"manage-group/middlewares"
	"manage-group/models"
	"github.com/gin-gonic/gin"
	"gorm.io/gorm"
)

func Migrate(db *gorm.DB) {
	db.AutoMigrate(&models.User{})
	db.AutoMigrate(&models.Group{})
	db.AutoMigrate(&models.UserGroup{})
	db.AutoMigrate(&models.Auth{})
}

func main() {
	db := database.Connect()
	Migrate(db)
	// Close
	defer database.CloseDB()
	router := initRouter()
	router.Run(":8080")
}

func initRouter() *gin.Engine {
	router := gin.Default()
	api := router.Group("/api")
	{
		api.POST("/user/register", controllers.RegisterUser)
		api.POST("/user/login", controllers.LoginUser)
		secured := api.Group("/").Use(middlewares.AuthMiddleware(true))
		{
			secured.GET("/profile", controllers.UserRetrieve)
			secured.PATCH("/profile", controllers.UserUpdate)
			secured.PATCH("/profile/change-password", controllers.ChangePassword)
			secured.POST("/user/logout", controllers.Logout)
			secured.POST("/group", controllers.CreateGroup)
			secured.GET("/group/:id", controllers.GroupRetrieve)
			secured.POST("/group/add-user", controllers.AddUserGroup)
			secured.DELETE("/group/remove-user", controllers.RemoveUserGroup)
		}
	}
	return router
}