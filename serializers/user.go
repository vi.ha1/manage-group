package serializers

import (
	"manage-group/auth"
	"manage-group/models"
	"github.com/gin-gonic/gin"
	"manage-group/database"
	// "net/http"
	// "strconv"
	// "fmt"
	//"time"
)

type UserSerializer struct {
	C *gin.Context
}

type UserResponse struct {
	ID uint `json:"id"`
	Username string  `json:"username"`
	Email    string  `json:"email"`
	Token    string  `json:"token"`
}

func (s *UserSerializer) Response() UserResponse{
	myUserModel := s.C.MustGet("my_user_model").(models.User)
	authD := s.C.MustGet("authD").(auth.JWTClaim)
	token, _ := auth.GenerateJWT(authD)
	user := UserResponse{
		ID : myUserModel.ID,
		Username: myUserModel.Username,
		Email: myUserModel.Email,
		Token: token,
	}
	return user
}

type ProfileSerializer struct {
	C *gin.Context
	models.User
}

type ProfileGroupSerializer struct {
	C *gin.Context
	models.User
}

type ProfilesGroupSerializer struct {
	C *gin.Context
	Users []models.User
}

type OwnerSerializer struct {
	C *gin.Context
	models.User
}

type UserBase struct{
	ID       uint    `json:"id"`
	Username string  `json:"username"`
}

type OwnerResponse struct {
	UserBase
}

type ProfileGroupResponse struct {
	UserBase
	// DateJoined time.Time
}

type ProfileResponse struct {
	UserBase
	Email    string  `json:"email"`
	GroupMn []GroupBase `json:"group_manage"`
	Groups []GroupProfileResponse `json:"list_group"`
}

func (s *ProfileSerializer) Response() ProfileResponse{
	var groups []models.Group
	grErr := database.GetDB().Find(&groups, "owner_id = ?", s.ID).Error
	if grErr != nil {
		return ProfileResponse{}
	}
	profile := ProfileResponse {
		UserBase : UserBase{
			ID : s.ID,
			Username : s.Username,
		},
		Email: s.Email,
		GroupMn : []GroupBase{},
		Groups : []GroupProfileResponse{},
	}
	grSerializer := GroupsSerializer{s.C, s.Groups}
	mnSerializer := GrUserMnSerializer{s.C, groups}
	profile.Groups = grSerializer.Response()
	profile.GroupMn = mnSerializer.Response()
	return profile
}

func (s *ProfileGroupSerializer) Response() ProfileGroupResponse{
	profile := ProfileGroupResponse {
		UserBase : UserBase {
			ID : s.ID,
			Username : s.Username,
		},
		// DateJoined : s.DateJoined,
	}
	return profile
}

func (s *ProfilesGroupSerializer) Response() []ProfileGroupResponse{
	response := []ProfileGroupResponse{}
	for _, user := range s.Users {
		serializer := ProfileGroupSerializer{s.C, user}
		response = append(response, serializer.Response())
	}
	return response
}

func (s *OwnerSerializer) Response() OwnerResponse{
	owner := OwnerResponse {
		UserBase : UserBase{
			ID : s.ID,
			Username : s.Username,
		},
	}
	return owner
}