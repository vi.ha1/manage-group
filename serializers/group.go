package serializers

import (
	"manage-group/models"
	"github.com/gin-gonic/gin"
	"manage-group/database"
)

type GroupSerializer struct {
	C *gin.Context
	models.Group
}

type GroupProfileSerializer struct {
	C *gin.Context
	models.Group
}

type GroupsSerializer struct {
	C *gin.Context
	Groups []models.Group
}

//GroupUserManageSerializer
type GrUserMnSerializer struct {
	C *gin.Context
	Groups []models.Group
}

type GroupBase struct {
	ID uint `json: "id"`
	Groupname string `json:"name"`
}

type GroupProfileResponse struct {
	GroupBase
	Owner OwnerResponse `json:"owner"`
}

type GroupResponse struct {
	GroupProfileResponse
	Members []ProfileGroupResponse `json:"list_member"`
}

//GroupUserManageResponse
type GrUserMnResponse struct {
	Groups []GroupBase `json:"group_manage"`
}

func (s *GroupSerializer) Response() GroupResponse {
	var user models.User
	userErr := database.GetDB().Find(&user, "id = ?", s.OwnerId).Error
	if userErr != nil {
		return GroupResponse{}
	}
	response := GroupResponse{
		GroupProfileResponse : GroupProfileResponse{
			GroupBase: GroupBase{
				ID : s.ID,
				Groupname : s.GroupName,
			},
			Owner :  OwnerResponse{},
		},
		Members : []ProfileGroupResponse{},
	}
	ownerSerializer := OwnerSerializer{s.C, user}
	profilesSerializer := ProfilesGroupSerializer{s.C, s.Members}
	response.GroupProfileResponse.Owner = ownerSerializer.Response()
	response.Members = profilesSerializer.Response()
	return response
}

func (s *GroupProfileSerializer) Response() GroupProfileResponse {
	var user models.User
	userErr := database.GetDB().Find(&user, "id = ?", s.OwnerId).Error
	if userErr != nil {
		return GroupProfileResponse{}
	}
	response := GroupProfileResponse{
		GroupBase : GroupBase{
			ID : s.ID,
			Groupname : s.GroupName,
		},
		Owner: OwnerResponse{},
	}
	serializer := OwnerSerializer{s.C, user}
	response.Owner = serializer.Response()
	return response
}

func (s *GroupsSerializer) Response() []GroupProfileResponse {
	response := []GroupProfileResponse{}
	for _, group := range s.Groups {
		serializer := GroupProfileSerializer{s.C, group}
		response = append(response, serializer.Response())
	}
	return response
}

func (s *GrUserMnSerializer) Response() []GroupBase {
	response := []GroupBase{}
	for _, group := range s.Groups {
		serializer := GroupBase{
			ID : group.ID,
			Groupname : group.GroupName,
		}
		response = append(response, serializer)
	}
	return response
}