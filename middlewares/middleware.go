package middlewares

import (
	"manage-group/models"
	"manage-group/auth"
	"manage-group/database"
	"github.com/gin-gonic/gin"
	//"fmt"
	"strings"
	"github.com/dgrijalva/jwt-go"
	"github.com/dgrijalva/jwt-go/request"
	"net/http"
)


func UpdateContextUserModel(c *gin.Context, auth auth.JWTClaim) {
	var myUserModel models.User
	if auth.UserId != 0 {
		db := database.GetDB()
		db.First(&myUserModel, auth.UserId)
	}
	c.Set("my_user_id", auth.UserId)
	c.Set("my_user_model", myUserModel)
	c.Set("authD", auth)
}

func stripBearerPrefixFromTokenString(tok string) (string, error) {
	// Should be a bearer token
	if len(tok) > 5 && strings.ToUpper(tok[0:6]) == "TOKEN " {
		return tok[6:], nil
	}
	return tok, nil
}

var AuthorizationHeaderExtractor = &request.PostExtractionFilter{
	request.HeaderExtractor{"Authorization"},
	stripBearerPrefixFromTokenString,
}

var MyAuth2Extractor = &request.MultiExtractor{
	AuthorizationHeaderExtractor,
	request.ArgumentExtractor{"access_token"},
}

func AuthMiddleware(auto401 bool) gin.HandlerFunc {
	return func(c *gin.Context) {
		UpdateContextUserModel(c, auth.JWTClaim{})
		token, err := request.ParseFromRequest(c.Request, MyAuth2Extractor, func(token *jwt.Token) (interface{}, error) {
			b := ([]byte(auth.JwtKey))
			return b, nil
		})
		if err != nil {
			if auto401 {
				c.AbortWithError(http.StatusUnauthorized, err)
			}
			return
		}
		if claims, ok := token.Claims.(jwt.MapClaims); ok && token.Valid {
			userId := uint(claims["user_id"].(float64))
			authUuid := claims["auth_uuid"].(string) //convert the interface to string

			auth := auth.JWTClaim{
				AuthUuid : authUuid,
				UserId : userId,
			}
			
			UpdateContextUserModel(c, auth)
		}
	}
}