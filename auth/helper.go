package auth
import (
	"errors"
	"time"
	"github.com/dgrijalva/jwt-go"
	"fmt"
)
const JwtKey = "supersecretkey"

type JWTClaim struct {
	//ID uint `json:"user_id"`
	AuthUuid string `json:"auth_uuid"`
	UserId   uint `json:"user_id"`
	jwt.StandardClaims
}
func GenerateJWT(auth JWTClaim) (tokenString string, err error) {
	expirationTime := time.Now().Add(1 * time.Hour)
	claims:= &JWTClaim{
		UserId: uint(auth.UserId),
		AuthUuid: auth.AuthUuid,
		StandardClaims: jwt.StandardClaims{
			ExpiresAt: expirationTime.Unix(),
		},
	}
	token := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)
	tokenString, err = token.SignedString([]byte(JwtKey))
	return
}
func ValidateToken(signedToken string) (err error) {
	token, err := jwt.ParseWithClaims(
		signedToken,
		&JWTClaim{},
		func(token *jwt.Token) (interface{}, error) {
			return []byte(JwtKey), nil
		},
	)
	if err != nil {
		return
	}
	claims, ok := token.Claims.(*JWTClaim)
	if !ok {
		err = errors.New("couldn't parse claims")
		return
	}
	if claims.ExpiresAt < time.Now().Local().Unix() {
		err = errors.New("token expired")
		return
	}
	fmt.Println(claims)
	return
}

// func ExtractTokenAuth(r *http.Request) (*JWTClaim, error) {
// 	token, err := VerifyToken(r)
// 	if err != nil {
// 		return nil, err
// 	}
// 	claims, ok := token.Claims.(jwt.MapClaims) //the token claims should conform to MapClaims
// 	if ok && token.Valid {
// 		authUuid, ok := claims["auth_uuid"].(string) //convert the interface to string
// 		if !ok {
// 			return nil, err
// 		}
// 		userId, err := strconv.ParseUint(fmt.Sprintf("%.f", claims["user_id"]), 10, 64)
// 		if err != nil {
// 			return nil, err
// 		}
// 		return &JWTClaim{
// 			AuthUuid: authUuid,
// 			UserId:   userId,
// 		}, nil
// 	}
// 	return nil, err
// }