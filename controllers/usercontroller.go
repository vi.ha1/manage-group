package controllers
import (
	"manage-group/auth"
	"manage-group/database"
	"manage-group/models"
	"manage-group/serializers"
	"manage-group/middlewares"
	"net/http"
	"github.com/gin-gonic/gin"
	"log"
	"fmt"
	"golang.org/x/crypto/bcrypt"
)

type LoginRequest struct {
	Username    string `json:"username"`
	Password string `json:"password"`
}

type UpdateRequest struct {
	Username    string `json:"username"`
	Email    string `json:"email"`
}

type ChangePasswordRequest struct {
	Password string `json:"password"`
}

func RegisterUser(context *gin.Context) {
	var user models.User
	if err := context.ShouldBindJSON(&user); err != nil {
		context.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		context.Abort()
		return
	}
	if err := user.HashPassword(user.Password); err != nil {
		context.JSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
		context.Abort()
		return
	}
	record := database.DB.Create(&user)
	if record.Error != nil {
		context.JSON(http.StatusInternalServerError, gin.H{"error": record.Error.Error()})
		context.Abort()
		return
	}
	// context.Set("my_user_model", user)
	// serializer := serializers.UserSerializer{C:context}
	context.JSON(http.StatusCreated, gin.H{"success": "register success!"})
}

func LoginUser(context *gin.Context) {
	var request LoginRequest
	var user models.User
	if err := context.ShouldBindJSON(&request); err != nil {
		context.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		context.Abort()
		return
	}
	record := database.DB.Where("username = ?", request.Username).First(&user)
	if record.Error != nil {
		context.JSON(http.StatusInternalServerError, gin.H{"error": "invalid username or password!"})
		context.Abort()
		return
	}

	credentialError := user.CheckPassword(request.Password)

	if credentialError != nil {
		context.JSON(http.StatusUnauthorized, gin.H{"error": "invalid username or password!"})
		context.Abort()
		return
	}
	_, fetchErr := models.FetchAuthExist(user.ID)
	if fetchErr == nil {
		dellError := models.DeleteAuthExist(user.ID)
		if dellError != nil {
			context.Abort()
			return
		}
	}
	//since after the user logged out, we destroyed that record in the database so that same jwt token can't be used twice. We need to create the token again
	authData, _ := models.CreateAuth(user.ID)
	authD := auth.JWTClaim{
		UserId : authData.UserID,
		AuthUuid : authData.AuthUUID,
	}

	middlewares.UpdateContextUserModel(context, authD)
	serializer := serializers.UserSerializer{C:context}
	context.JSON(http.StatusOK, gin.H{"user": serializer.Response()})

}

func UserUpdate(context *gin.Context) {
	//Check authorization
	au := context.MustGet("authD").(auth.JWTClaim)
	authD, err := models.FetchAuth(au)
	if err != nil {
		context.JSON(http.StatusUnauthorized, "unauthorized")
		return
	}
	var request UpdateRequest
	if err := context.ShouldBindJSON(&request); err != nil {
		context.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		context.Abort()
		return
	}

	db := database.GetDB()
	var user models.User
	
	userErr := db.Find(&user, "id = ?", authD.UserID).Error
	if userErr != nil {
		context.JSON(http.StatusInternalServerError, gin.H{"error": userErr.Error()})
		context.Abort()
		return
	}

	if (request.Username == "" || request.Email == "") {
		context.JSON(http.StatusBadRequest, gin.H{"error": "invalid information!"})
		return
	}

	updateErr := db.Debug().Model(&user).Updates(models.User{Username: request.Username, Email: request.Email}).Error
	if userErr != nil {
		context.JSON(http.StatusInternalServerError, gin.H{"error": updateErr.Error()})
		context.Abort()
		return
	}

	serializer := serializers.ProfileSerializer{context, user}
	context.JSON(http.StatusOK, gin.H{"user": serializer.Response()})
}

func UserRetrieve(context *gin.Context) {
	//Check authorization
	au := context.MustGet("authD").(auth.JWTClaim)
	authD, err := models.FetchAuth(au)
	if err != nil {
		context.JSON(http.StatusUnauthorized, "unauthorized")
		return
	}

	db := database.GetDB()
	var user models.User
	
	userErr := db.Preload("Groups").Find(&user, "id = ?", authD.UserID).Error
	if userErr != nil {
		context.JSON(http.StatusInternalServerError, gin.H{"error": userErr.Error()})
		context.Abort()
		return
	}
	serializer := serializers.ProfileSerializer{context, user}
	context.JSON(http.StatusOK, gin.H{"user": serializer.Response()})
}


func Logout(context *gin.Context) {
	fmt.Println(context.Request)
	au := context.MustGet("authD").(auth.JWTClaim)
	delErr := models.DeleteAuth(au)
	if delErr != nil {
		log.Println(delErr)
		context.JSON(http.StatusUnauthorized, "unauthorized")
		return
	}
	context.JSON(http.StatusOK, "Successfully logged out")
}

func ChangePassword(context *gin.Context) {
	//Check authorization
	au := context.MustGet("authD").(auth.JWTClaim)
	authD, err := models.FetchAuth(au)
	if err != nil {
		context.JSON(http.StatusUnauthorized, "unauthorized")
		return
	}
	var request ChangePasswordRequest
	if err := context.ShouldBindJSON(&request); err != nil {
		context.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		context.Abort()
		return
	}

	db := database.GetDB()
	var user models.User
	
	userErr := db.Find(&user, "id = ?", authD.UserID).Error
	if userErr != nil {
		context.JSON(http.StatusInternalServerError, gin.H{"error": userErr.Error()})
		context.Abort()
		return
	}

	if request.Password == "" {
		context.JSON(http.StatusBadRequest, gin.H{"error": "invalid password!"})
		return
	}

	bytes, _ := bcrypt.GenerateFromPassword([]byte(request.Password), 14)

	updateErr := db.Debug().Model(&user).Updates(models.User{Password: string(bytes)}).Error
	if userErr != nil {
		context.JSON(http.StatusInternalServerError, gin.H{"error": updateErr.Error()})
		context.Abort()
		return
	}

	context.JSON(http.StatusOK, gin.H{"success": "password updated!"})
}
