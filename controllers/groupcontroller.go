package controllers
import (
	"manage-group/auth"
	"manage-group/database"
	"manage-group/models"
	"manage-group/serializers"
	"net/http"
	"github.com/gin-gonic/gin"
	//"fmt"
	//"reflect"
	"time"
)

type UserGroupRequest struct {
	UserId []uint `json:"user"`
	GroupId uint `json:"group"`
}

func CreateGroup(context *gin.Context) {
	//Check authorization
	au := context.MustGet("authD").(auth.JWTClaim)
	authD, err := models.FetchAuth(au)
	if err != nil {
		context.JSON(http.StatusUnauthorized, "unauthorized")
		return
	}

	var group models.Group
	if err := context.ShouldBindJSON(&group); err != nil {
		context.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		context.Abort()
		return
	}
	group.OwnerId = authD.UserID
	record := database.GetDB().Create(&group)
	
	if record.Error != nil {
		context.JSON(http.StatusInternalServerError, gin.H{"error": "name of group has already exists!"})
		context.Abort()
		return
	}
	serializer := serializers.GroupSerializer{context, group}
	context.JSON(http.StatusCreated, gin.H{"group": serializer.Response()})
}

func GroupRetrieve(context *gin.Context) {
	//Check authorization
	au := context.MustGet("authD").(auth.JWTClaim)
	if _, err := models.FetchAuth(au); err != nil {
		context.JSON(http.StatusUnauthorized, "unauthorized")
		return
	}

	db := database.GetDB()
	var group models.Group
	groupId := context.Param("id")
	record := db.Preload("Members").Find(&group, "id = ?", groupId)
	if record.Error != nil {
		context.JSON(http.StatusInternalServerError, gin.H{"error": record.Error.Error()})
		context.Abort()
		return
	}

	serializer := serializers.GroupSerializer{context, group}
	context.JSON(http.StatusOK, gin.H{"group": serializer.Response()})
}

func AddUserGroup(context *gin.Context) {
	//Check authorization
	au := context.MustGet("authD").(auth.JWTClaim)
	authD, err := models.FetchAuth(au)
	if err != nil {
		context.JSON(http.StatusUnauthorized, "unauthorized")
		return
	}

	var request UserGroupRequest
	var group models.Group
	var members []models.User
	//requestUserId := uint(context.MustGet("my_user_id").(uint))
	db := database.GetDB()
	if err := context.ShouldBindJSON(&request); err != nil {
		context.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		context.Abort()
		return
	}
	
	for _, id := range request.UserId {	
		var user models.User
		err := db.Where("id = ?", id).First(&user).Error
		if err != nil {
			context.JSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
			context.Abort()
			return
		}
		members = append(members, user)
	}

	if err := db.Find(&group, "id = ?", request.GroupId).Error; err != nil {
		context.JSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
		context.Abort()
		return
	}

	if group.OwnerId != authD.UserID {
		context.JSON(http.StatusBadRequest, gin.H{"error": "you don't have permission to add user to group"})
		return
	}

	err = AssignUserToGroup(group, members)
	if err != nil {
		context.JSON(http.StatusBadRequest, gin.H{"error": "user is already joined group"})
		return
	}
	context.JSON(http.StatusCreated, gin.H{"success": "add user to group success!"})

}

func AssignUserToGroup(group models.Group, members []models.User) (err error) {
	db := database.GetDB()
	for _, member := range members{
		err := db.Create(&models.UserGroup{
			UserId: member.ID,
			GroupId:   group.ID,
			DateJoined: time.Now(),
		}).Error

		if err != nil {
			db.Rollback()
        	return err
		}
	}
	return nil
}

func RemoveUserGroup(context *gin.Context) {
	//Check authorization
	au := context.MustGet("authD").(auth.JWTClaim)
	authD, err := models.FetchAuth(au)
	if err != nil {
		context.JSON(http.StatusUnauthorized, "unauthorized")
		return
	}

	var request UserGroupRequest
	var group models.Group
	var members []models.User
	db := database.GetDB()
	if err := context.ShouldBindJSON(&request); err != nil {
		context.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		context.Abort()
		return
	}
	
	for _, id := range request.UserId {	
		var user models.User
		err := db.Where("id = ?", id).First(&user).Error
		if err != nil {
			context.JSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
			context.Abort()
			return
		}
		members = append(members, user)
	}

	if err := db.Find(&group, "id = ?", request.GroupId).Error; err != nil {
		context.JSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
		context.Abort()
		return
	}

	if group.OwnerId != authD.UserID {
		context.JSON(http.StatusBadRequest, gin.H{"error": "you don't have permission to remove user from group"})
		return
	}

	err = RemoveUserFromGroup(group, members)
	if err != nil {
		context.JSON(http.StatusBadRequest, gin.H{"error": "user is not exist in group"})
		return
	}

	context.JSON(http.StatusOK, gin.H{"success": "remove user success!"})

}

func RemoveUserFromGroup(group models.Group, members []models.User) (err error) {
	db := database.GetDB()
	userGr := &models.UserGroup{}
	for _, member := range members{
		err := db.Debug().Where("group_id = ? AND user_id = ?", group.ID, member.ID).Delete(&userGr).Error
		if err != nil {
			db.Rollback()
        	return db.Error
		}
	}
	return nil
}